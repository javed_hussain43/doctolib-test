# Problem Statement
The goal is to write an algorithm that checks the availabilities of an agenda depending of the events attached to it. The main method has a start date for input and is looking for the availabilities of the next 7 days.

They are two kinds of events:

opening, are the openings for a specific day and they can be recurring week by week.
appointment, times when the doctor is already booked.


#Assumptions

i)   Openings and appointments must not exceed a single day
ii)  Slots are of a fixed size of 30 minutes
iii) recurring opening event starts to be effective only after its actual date


#Project Installation
Ruby Version  - 2.6.6
Rails version - 6.0.2.2

git clone GIT_URL

cd doctolib-test

/doctolib-test bundle install && rails db:setup

#How to Run test cases

rails test test/models/event_test.rb